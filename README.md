# CoxAppointments

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.

## Development server

Run `npm install` after downloading this project from the git repository --- https://gitlab.com/jtpoole24/appointments-cox.git

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Details

This project was developed by Justin Poole on 2/27/2019 for the purpose of a coding assessment for Cox Automotive.

jtpoole24@gmail.com