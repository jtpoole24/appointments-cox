import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptModalComponent } from './appt-modal.component';

describe('ApptModalComponent', () => {
  let component: ApptModalComponent;
  let fixture: ComponentFixture<ApptModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApptModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApptModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
