// this component controls the opening of the modal along with the passing of data from the list of appointment buttons


import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AppointmentDataService } from '../appointment-data.service';

@Component({
  selector: 'appt-modal',
  templateUrl: 'appt-modal.component.html',
  styleUrls: ['appt-modal.component.css'],
})
export class ApptModalComponent {

  constructor(public dialog: MatDialog, private appt: AppointmentDataService) {}

  // when an appointment button is clicked, the openDialog method opens the modal window, and passes the index of the button
  // that was clicked into the modal, so that the modal can access the data of the particular appointment
  openDialog(i: number): void {
    this.appt.appts[i].available = false;
    const dialogRef = this.dialog.open(ApptModalDialog, {
      width: '450px',
      data: i
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }

}

// this component controls the modal dialog window, there are two components in this file because they are intentionally coupled to one
// another...

@Component({
  selector: 'appt-modal-dialog',
  templateUrl: 'appt-modal-dialog.html',
})
export class ApptModalDialog {

  // the AppointmentDataService, which contains the array of appointments, along with the data (button index that was clicked)
  // are both injected into this constructor
  constructor(public dialogRef: MatDialogRef<ApptModalDialog>, private appt: AppointmentDataService,
    @Inject(MAT_DIALOG_DATA) public data){}

  closeModal(): void {
    this.dialogRef.close();
  }

}
