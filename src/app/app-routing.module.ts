import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApptModalComponent } from './appt-modal/appt-modal.component';

// the only valid route of this application will navigate you to the ApptModalComponent, which displays the list of appointment buttons
// that will open the dialog window of a particular appointment
const routes: Routes = [
  { path: '**', component: ApptModalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
