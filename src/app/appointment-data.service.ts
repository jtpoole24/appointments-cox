// this service defines the interface of what an Appointment object should look like, and also hard-codes an array of
// Appointments

import { Injectable } from '@angular/core';

export interface Appointment {
  available: boolean,
  time: number,
  name: string,
  phoneNumber: number,
}


@Injectable({
  providedIn: 'root'
})
export class AppointmentDataService {

  constructor() { }

  appts: Appointment[] = [
    {
      available: true,
      time: 9,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 10,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 11,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 12,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 1,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 2,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 3,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 4,
      name: "",
      phoneNumber: 7777777777
    },
    {
      available: true,
      time: 5,
      name: "",
      phoneNumber: 7777777777
    }
]
}
